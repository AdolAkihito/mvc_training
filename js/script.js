$( document ).ready(function() {
    var quotes = $('.quote');
    quotes.hide();

    var qlen = quotes.length; //document.write( random(qlen-1) );
    $( '.quote:eq(' + random(qlen-1) + ')' ).show(); //tag:eq(1)

    $(".task").click(
    function(){
            sendAjaxForm('result_form', 'ajax_form');
        }
        );
});

function sendAjaxForm(result_form, ajax_form) {
    let vall = $('input[name="task"]:checked').val();
    $.ajax({
        url:     '/index/ajax_form',
        type:     "GET",
        dataType: "html",
        data: {task : vall},
        success: function(response) { //Данные отправлены успешно
            $('#result_form').html(response);
        },
        error: function(response) { // Данные не отправлены
            $('#result_form').html('Ошибка. Данные не отправлены.');
        }
    });
}
// return a random integer between 0 and number
function random(number) {
    return Math.floor( Math.random()*(number+1) );
}
