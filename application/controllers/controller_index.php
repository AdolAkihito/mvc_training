<?php

namespace Application\Controllers;

use Application\Core\Controller;
use Application\Core\View;
class Controller_Index extends Controller
{
    public function __construct()
    {
        $routes = explode('/', $_SERVER['REQUEST_URI']);
        $model_name = 'task';

        if (file_exists("application/models/model_" . $model_name . ".php")) {
            $model_name = ucfirst($model_name);
            $model_name = 'Application\Models\Model_' . $model_name;
            $this->model = new $model_name();
        }
        $this->view = new View();
    }

    public function action_index()
    {
        $name_view = 'main';
        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[1])) {
            $name_view = $routes[1];
        }

        $name_view_php = $name_view . '_view.php';

        if (file_exists("application/models/model_" . $name_view . '.php')) {
            $data = $this->model->get_data();
            $this->view->generate($name_view_php, 'template_view.php', $data);
        } else {

            $this->view->generate($name_view_php, 'template_view.php');
        }
    }

    public function action_task1()
    {
        $data = $this->model->task1();
        $this->view->generate('task1_view.php','template_view.php', $data);
    }

    public function action_task2()
    {
        $data = $this->model->task2();
        $this->view->generate('task2_view.php','template_view.php', $data);
    }

    public function action_task3()
    {
        $data = $this->model->task3();
        $this->view->generate('task3_view.php', 'template_view.php', $data);
    }

    public function action_task4()
    {
        $data = $this->model->task4();
        $this->view->generate('task4_view.php','template_view.php', $data);
    }

    public function action_task5()
    {
        $data = $this->model->task5();
        $this->view->generate('task5_view.php','template_view.php', $data);
    }

    public function action_task6()
    {
        $data = $this->model->task6();
        $this->view->generate('task6_view.php','template_view.php', $data);
    }

    public function action_404()
    {
        $this->view->generate('404_view.php','template_view.php');
    }
    public function action_ajax_form()
    {
        $model_name = $_GET['task'];
        $data = $this->model->$model_name();
        $name_view = $_GET['task'];
        $name_view = $name_view . '_view.php';
        $this->view->create($name_view, $data);
    }
}