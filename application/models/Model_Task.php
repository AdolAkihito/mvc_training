<?php
namespace Application\Models;

class Model_Task {

    function task1() {
        $max_rand = 9;
        $min_rand = 0;
        $midNum = ($min_rand + $max_rand) / 2;
        $randNum = mt_rand(0, $max_rand);
        if ($randNum % 2 === 0) {
            $even = "Чётное";
        } else {
            $even = "Нечётное";
        }
        if ($randNum === $midNum) {
            $midNumRes = "Число является серединой диапазона";
        } else {
            $midNumRes = "Число не является серединой диапазона";
            if ($randNum > $midNum) {
                $midNumRes .= "<br>Число больше среднего";
            } else {
                $midNumRes .= "<br>Число меньше среднего";
            }
        }
        return [$randNum, $even, $midNumRes];
    }

    function task2() {
        $arr = [];
        $exp = 0;
        while ($exp <= 10) {
            $randNum = mt_rand(0, 100);
            array_push($arr, $randNum);
            $exp++;
        }
        asort($arr);
        $result = implode(", ", $arr);
        return $result;
    }

    function task3() {
        $num1 = random_int(0, 100);
        $num2 = random_int(0, 100);
        $midNum = ($num1 + $num2) / 2;
        $sum = $num1 + $num2;
        $app = $num1 * $num2;
        $dif = abs($num1 - $num2);
        $app1 = $app;
        $dif1 = $dif;
        $sum1 = $sum;

        if ($sum1 === $midNum) {
            $sum .= "Сумма является серединой диапазона\n";
        } else {
            $sum .= "Сумма не является серединой диапазона\n";
            if ($sum1 > $midNum) {
                $sum .= "Сумма больше среднего\n";
            } else {
                $sum .= "Сумма меньше среднего\n";
            }
        }
        if ($app1 === $midNum) {
            $app .= "Произведение является серединой диапазона\n";
        } else {
            $app .= "Произведение не является серединой диапазона\n";
            if ($app1 > $midNum) {
                $app .= "Произведение больше среднего\n";
            } else {
                $app .= "Произведение меньше среднего\n";
            }
        }
        if ($dif1 === $midNum) {
            $dif .= "Разница является серединой диапазона\n";
        } else {
            $dif .= "Разница не является серединой диапазона\n";
            if ($dif1 > $midNum) {
                $dif .= "Разница больше среднего\n";
            } else {
                $dif .= "Разница меньше среднего\n";
            }
        }
        return [$num1, $num2, $sum, $app, $dif];
    }

    function task4() {
        $arr = [];
        $exp = 0;
        while($exp < 17){
            $num1 = mt_rand(0, 10);
            array_push($arr, $num1);
            $exp++;
        }
        $arr_uni = array_unique($arr);
        return [$arr, $arr_uni];
    }

    function task5() {
        $mounths = ['January', 'February', 'March', 'April', 'May', 'June', 'Jule', 'August', 'September', 'October', 'November','December'];
        $maxLength = null;
        $maxLengthIndex = null;
        $minLength = null;
        $minLengthIndex = null;
        $date = date('F');

        foreach ($mounths as $index => $value) {
            $length = strlen($value);
            if (is_null($maxLength) || $length > $maxLength) {
                $maxLength = $length;
                $maxLengthIndex = $index;
            }
        }

        foreach ($mounths as $index => $value) {
            $length = strlen($value);
            if (is_null($minLength) || $length < $minLength) {
                $minLength = $length;
                $minLengthIndex = $index;
            }
        }

        $minLenghtMounth = 'Месяц название которого имеет самую короткую длину строки' . $mounths[$minLengthIndex];
        $indexMounth = "Индекс текущего месяца "  . array_search($date, $mounths);
        $maxLenghtMounth = 'Месяц название которого имеет самую большую длину строки ' . $mounths[$maxLengthIndex];
        return [$minLenghtMounth, $indexMounth, $maxLenghtMounth];
    }

    function task6() {
        $mounths = ['January', 'February', 'March', 'April', 'May', 'June', 'Jule', 'August', 'September', 'October', 'November', 'December'];
        $arrKey = [];
        $arrDay = array(31 => "", 29 => "", 30 => "");
        foreach ($mounths as $index => $value) {
            $index1 = $index + 1;
            $number = cal_days_in_month(CAL_GREGORIAN, $index1, 2020);
            $arrKey[$value] = $number;
        }
        foreach ($arrKey as $index => $value) {
            $arrDay[$value] .= $index . " ";
        }
        return [$arrKey, $arrDay];
    }
}